#include "../inc/Fixed.hpp"

Fixed::Fixed(void) : 
    dot(0)
{
    std::cout<<"Default constructor called"<<std::endl;
}

Fixed::Fixed(Fixed const &other_fixed)
{
    std::cout<<"Copy constructor called"<<std::endl;
    setRawBits(other_fixed.getRawBits());
}

Fixed::~Fixed()
{
    std::cout<<"Destructor called"<<std::endl;
}

int Fixed::getRawBits(void) const
{
    std::cout<<"getRawBits member function called"<<std::endl;
    return this->dot;
}

void Fixed::setRawBits(int const raw)
{
    this->dot = raw;
}

Fixed& Fixed::operator= (Fixed const &f)
{

	std::cout<<"Assignation operator called"<<std::endl;
	if(this!=&f)
	{
		this->dot = f.getRawBits();
	}

	return *this;
}
