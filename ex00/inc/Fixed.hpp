#ifndef FIXED_HPP_
#define FIXED_HPP_

#include <iostream>

class Fixed 
{
    private:
        
        int dot;
        static const int fractionals = 8;
    public:
        Fixed(void);
        Fixed(Fixed const &other_fixed);
        ~Fixed();

        int getRawBits(void) const;
        void setRawBits(int const raw);
		Fixed& operator= (Fixed const &f);
};
#endif
