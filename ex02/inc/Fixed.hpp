#ifndef FIXED_HPP_
#define FIXED_HPP_

#include <iostream>
#include <math.h>

class Fixed 
{
    private:
        
        int _value;
        static const int _fractionals = 8;
    public:
        Fixed(void);
        Fixed(Fixed const &other_fixed);
		Fixed(const int num);
		Fixed(const float float_num);
        ~Fixed();

        int getRawBits(void) const;
        void setRawBits(int const raw);
		int toInt(void) const;
		float toFloat(void) const;
		Fixed& operator= (Fixed const &f);
        bool operator == (Fixed const &f);
        bool operator != (Fixed const &f);
        bool operator > (Fixed const &f);
        bool operator < (Fixed const &f);
        bool operator >= (Fixed const &f);
        bool operator <= (Fixed const &f);
        Fixed operator + (Fixed const &f2);
        Fixed operator - (Fixed const &f2);
        Fixed operator * (Fixed const &f2);
        Fixed operator / (Fixed const &f2);
        Fixed& operator ++(); 
        Fixed operator ++(int);
        Fixed& operator --();
        Fixed operator --(int);
        static Fixed& min(Fixed &f1, Fixed &f2);
        static const Fixed& min(Fixed const &f1, Fixed const &f2);
        static Fixed& max(Fixed &f1, Fixed &f2);
        static const Fixed& max(Fixed const &f1, Fixed const &f2);
};


		std::ostream& operator << (std::ostream &o, Fixed const &f);
#endif
