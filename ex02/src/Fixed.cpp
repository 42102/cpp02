#include "../inc/Fixed.hpp"

Fixed::Fixed(void) : 
    _value(0)
{
}

Fixed::Fixed(Fixed const &other_fixed)
{
   	*this = other_fixed;
}

Fixed::Fixed(const int num)
{
	this->_value = num<<(this->_fractionals);	
}

Fixed::Fixed(const float float_num)
{
	this->_value = (int)roundf(float_num * (1 <<this->_fractionals));
}
Fixed::~Fixed()
{
}

int Fixed::getRawBits(void) const
{
    return this->_value;
}

void Fixed::setRawBits(int const raw)
{
    this->_value = raw;
}

int Fixed::toInt(void) const
{
	return ((int)this->_value>>(this->_fractionals));	
}

float Fixed::toFloat(void) const
{
	return ((float)this->_value / (float)(1<<this->_fractionals));
}

Fixed& Fixed::operator= (Fixed const &f)
{

	if(this!=&f)
	{
		this->_value = f.getRawBits();
	}

	return *this;
}


std::ostream& operator<< (std::ostream &o, Fixed const &f)
{
	(void)f;
	o<<f.toFloat();
	return o;
}

Fixed Fixed::operator + (Fixed const &f2)
{
   Fixed result;

    result._value = this->_value + f2._value;

   return result; 
}

Fixed Fixed::operator - (Fixed const &f2)
{
    Fixed result;

    result._value = this->_value - f2._value;
    
    return result;    
}

Fixed Fixed::operator * (Fixed const &f2)
{
    Fixed result (this->toFloat() * f2.toFloat());

    return result;
}

Fixed Fixed::operator / (Fixed const &f2)
{
    Fixed result(this->toFloat() / f2.toFloat());

    return result;
}

Fixed& Fixed::operator ++ ()
{
    this->_value += 1;
    return *this;
}


Fixed Fixed::operator ++ (int)
{ 
    Fixed temp;
    temp._value = this->_value + 1;
    return temp;
}

Fixed& Fixed::operator -- ()
{ 
    this->_value -= 1;
    return *this;
}

Fixed Fixed::operator -- (int)
{
    Fixed tmp;

    tmp._value = this->_value - 1;
    return tmp;
}

bool Fixed::operator == (Fixed const &f)
{
    return (this->_value == f._value);
}

bool Fixed::operator != (Fixed const &f)
{
    return (this->_value != f._value);
}

bool Fixed::operator > (Fixed const &f)
{
    return (this->_value > f._value);
}


bool Fixed::operator < (Fixed const &f)
{
    return (this->_value < f._value);
}


bool Fixed::operator >= (Fixed const &f)
{
    return (this->_value >= f._value);
}


bool Fixed::operator <= (Fixed const &f)
{
    return (this->_value <= f._value);
}

Fixed& Fixed::min(Fixed &f1, Fixed &f2)
{
   if(f1.getRawBits() <= f2.getRawBits())
       return f1;
   else
       return f2;
}


const Fixed& Fixed::min(Fixed const  &f1, Fixed const &f2)
{
   if(f1.getRawBits() <= f2.getRawBits())
       return f1;
   else
       return f2;
}


const Fixed& Fixed::max(const Fixed &f1, const Fixed &f2)
{
   if(f1.getRawBits() >= f2.getRawBits())
       return f1;
   else
       return f2;
}


Fixed& Fixed::max(Fixed &f1, Fixed &f2)
{
   if(f1.getRawBits() >= f2.getRawBits())
       return f1;
   else
       return f2;
}
