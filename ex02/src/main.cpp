#include "../inc/Fixed.hpp"
#include <iostream>

int main(void)
{
    
    Fixed a;
    Fixed const b( Fixed( 5.05f ) * Fixed( 2 ) ); 
    
    Fixed c(1.5f);
    Fixed d(1.49f);
    
    std::cout<<"Suma de c + 3.49 = "<<c + 3.49f<<" unidades"<<std::endl;
    std::cout<<"Resta de c - 3.49 = "<<c - 3.49f<<" unidades"<<std::endl;
    std::cout<<"Multiplicacion de c * 3.49 = "<<c * 3.49f<<" unidades"<<std::endl; 
    std::cout<<"Division de c / 0.5 = "<<c / 0.5f<<" unidades"<<std::endl;
    std::cout<<"c == d :"<<(c == d)<<std::endl; 
    std::cout<<"c != d :"<<(c != d)<<std::endl;
    std::cout<<"c >= d :"<<(c >= d)<<std::endl;
    std::cout<<"c <= d :"<<(c <= d)<<std::endl;
    std::cout<<"c > d :"<<(c > d)<<std::endl;
    std::cout<<"c < d :"<<(c < d)<<std::endl;
    
    std::cout << a << std::endl;
    std::cout << ++a << std::endl;
    std::cout << a << std::endl;
    std::cout << a++ << std::endl;
    std::cout << a << std::endl;
    
    std::cout << b << std::endl;
    
    std::cout << Fixed::max( a, b ) << std::endl;
	
    return 0;
}
