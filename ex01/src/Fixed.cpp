#include "../inc/Fixed.hpp"

Fixed::Fixed(void) : 
    _value(0)
{
    std::cout<<"Default constructor called"<<std::endl;
}

Fixed::Fixed(Fixed const &other_fixed)
{
    std::cout<<"Copy constructor called"<<std::endl;
   	*this = other_fixed;
}

Fixed::Fixed(const int num)
{
	std::cout<<"Int constructor called"<<std::endl;
	this->_value = num<<(this->_fractionals);	
}

Fixed::Fixed(const float float_num)
{
	std::cout<<"Float constructor called"<<std::endl;
	this->_value = (int)roundf(float_num * (1 <<this->_fractionals));
}
Fixed::~Fixed()
{
    std::cout<<"Destructor called"<<std::endl;
}

int Fixed::getRawBits(void) const
{
    return this->_value;
}

void Fixed::setRawBits(int const raw)
{
    this->_value = raw;
}

Fixed& Fixed::operator= (Fixed const &f)
{

	std::cout<<"Assignation operator called"<<std::endl;
	if(this!=&f)
	{
		this->_value = f.getRawBits();
	}

	return *this;
}


std::ostream& operator<< (std::ostream &o, Fixed const &f)
{
	(void)f;
	o<<f.toFloat();
	return o;
}


int Fixed::toInt(void) const
{
	return ((int)this->_value>>(this->_fractionals));	
}

float Fixed::toFloat(void) const
{
	return ((float)this->_value / (float)(1<<this->_fractionals));
}

