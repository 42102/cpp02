#ifndef FIXED_HPP_
#define FIXED_HPP_

#include <iostream>
#include <math.h>

class Fixed 
{
    private:
        
        int _value;
        static const int _fractionals = 8;
    public:
        Fixed(void);
        Fixed(Fixed const &other_fixed);
		Fixed(const int num);
		Fixed(const float float_num);
        ~Fixed();

        int getRawBits(void) const;
        void setRawBits(int const raw);
		int toInt(void) const;
		float toFloat(void) const;
		Fixed& operator= (Fixed const &f);
};


		std::ostream& operator << (std::ostream &o, Fixed const &f);
#endif
